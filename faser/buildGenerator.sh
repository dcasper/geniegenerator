#!/bin/bash

mkdir -p $GENIE/../run/bin
mkdir -p $GENIE/../run/lib
mkdir -p $GENIE/../run/include


cd $GENIE
./configure --enable-faser --prefix=$GENIE/../run --with-pythia6-lib=$PYTHIA6_PATH/lib --with-lhapdf5-lib=$LHAPDF5_PATH/lib --with-lhapdf5-inc=$LHAPDF5_PATH/inc #--disable-lhapdf5 --enable-lhapdf6 
make -j clean
make -j
make -j distclean
make -j install
